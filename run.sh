#!/bin/bash

sleep 1  # give some time for systemd to start

ps -fAHww

echo "running systemctl status"
systemctl status || echo "systemctl status failed"

echo "running systemctl status via ansible"
ansible --connection local localhost -m command -a "systemctl status" || echo "ansible local failed"
